import time
from datetime import date
import requests
from config import COUNTRIES_API, COVID_API

def get_countries(currency):
    print('Getting list of countries')

    response = requests.get(COUNTRIES_API+f'/currency/{currency}')
    if response.status_code < 400:
        countries = response.json()
        countries_list = [
            (country.get('name'), country.get('alpha2Code')) for country in countries
        ]
    else:
        print(response.url)
        print(response.status_code)
        print(response.json())
        countries_list = []

    return countries_list

def get_today_cases(country):
    print(f'Getting cases for country {country}')
    response = requests.get(COVID_API +f'/total/country/{country}')

    if response.status_code < 400:
        cases = response.json()

        if cases:
            today_cases = cases[-1]
            counts_of_events = {
                'confirmed': today_cases.get('Confirmed'),
                'deaths': today_cases.get('Deaths'),
                'recovered': today_cases.get('Recovered'),
                'active': today_cases.get('Active'),
            }
        else:
            counts_of_events = {'error': 'Info not available'}

    else:
        print(response.status_code)
        print(response.json())
        counts_of_events = {'error': 'API error'}

        
    return counts_of_events