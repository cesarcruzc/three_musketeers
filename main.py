import time
import asyncio
from flask import Flask
from long_task import get_countries, get_today_cases

app = Flask(__name__)

my_loop = asyncio.get_event_loop()

@app.route("/")
def welcome_message():
    welcome_message = f'Welcome to this COVID BY CURRENCIES restAPI'
    return app.send_static_file('index.html')


@app.route("/get_cases_today/<currency>")
def get_cases_today(currency):
    t = time.time()

    countries = get_countries(currency)
    result = [
        {'country':country[0],'cases': get_today_cases(country[1])} for country in countries
    ]

    print(result)
    t = time.time() - t 

    return {
        'result': result,
        'runtime': str(t)
    }