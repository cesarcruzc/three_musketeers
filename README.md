# Three_musketeers

The idea of this project is to present three different techniques/tecnologies in the python environment to address one problem, deal with many and costly operations in an API.

Nowadays is not rare to have to develop a REST API to provide service to other services and it is not rare the need to consume from other thirdparty APIs. Contrary to the operations in memory, the oparations or requests in which we have to access network interfaces (as well as with the disk access) have a significant delay.

# Motivation

# Structure of the repository/project

Each one of the branches contains the data we will go through. So we here is a brief description of the branches as we progress with the talk:

* **init** The branch that contains the initial implementation of our API. It is implemented with Flask as application server and using Gunicorn as web server.
* **async** Branch were we will stop briefly to explain some concepts related to concurrence and why is recommended in some cases. 
* **fastapi** Branch that covers the initial example but now migrated to fastAPI as application server and Uvicorn as web server.
* **celery** Branch that implement the case in which a distributed task could be needed.

# init branch

# async branch

# fastapi

# celery

# What from here...

# Relevant links

[Gunicorn](https://gunicorn.org/)
[Flask](https://flask.palletsprojects.com/en/1.1.x/)
[asyncio](https://docs.python.org/3/library/asyncio.html)
[fastAPI](https://fastapi.tiangolo.com/features/)
[uvicorn](https://www.uvicorn.org/)
[Starlette](https://www.starlette.io/)
[Pydantic](https://bahiz.zeelo.co/)
[Celery](https://docs.celeryproject.org/en/stable/)
[Redis](https://redis.io/)
